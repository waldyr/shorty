FROM ruby:2.5.0
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN mkdir /shorty
WORKDIR /shorty
COPY Gemfile /shorty/Gemfile
COPY Gemfile.lock /shorty/Gemfile.lock
RUN bundle install
COPY . /shorty
