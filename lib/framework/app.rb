# frozen_string_literal: true

require 'json'

module Framework
  class App
    def initialize(router)
      @router = router
    end

    def call(env)
      result = @router.serve(Rack::Request.new(env))

      if result
        render(result)
      else
        render(status: :not_found)
      end
    end

    private

    def render(result)
      status = result.fetch(:status)
      content = result[:content] || {}
      headers = result[:headers] || {}

      [
        Rack::Utils.status_code(status),
        default_headers.merge(headers),
        [content.to_json]
      ]
    end

    def default_headers
      {
        'Content-Type' => 'application/json'
      }
    end
  end
end
