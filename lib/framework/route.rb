# frozen_string_literal: true

module Framework
  class Route < Struct.new(:route_spec, :callable)
    def match(request)
      path_components = request.path.split('/')
      spec_components = route_spec.split('/')

      return unless path_components.length == spec_components.length

      params = {}

      path_components.zip(spec_components) do |path_comp, route_comp|
        if route_comp.start_with?(':')
          params[route_comp.sub(':', '')] = path_comp
        elsif path_comp != route_comp
          return
        end
      end

      params = params.merge(request.params)

      begin
        params = params.merge(JSON.parse(request.body.read))
      rescue JSON::ParserError
        # skip
      end

      callable.call(params)
    end
  end
end
