# frozen_string_literal: true

module Framework
  class Router
    def initialize(route_table)
      @routes = route_table
    end

    def serve(request)
      @routes.fetch(request.request_method, []).each do |route|
        result = route.match(request)
        return result if result
      end

      nil
    end
  end
end
