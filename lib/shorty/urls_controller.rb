# frozen_string_literal: true

require 'securerandom'
require 'time'

module Shorty
  class UrlsController
    SHORTCODE_REGEX = /\A[0-9a-zA-Z_]{4,}\z/.freeze

    def initialize(db)
      @db = db[:shortcodes]
    end

    def shorten(params)
      url = params['url']
      shortcode = params['shortcode'] || SecureRandom.alphanumeric(6)

      if url.nil? || url.empty?
        { status: :bad_request }
      elsif @db.first(shortcode: params['shortcode'])
        { status: :conflict }
      elsif SHORTCODE_REGEX.match?(shortcode)
        @db.insert(
          url: url,
          shortcode: shortcode,
          created_at: Time.now.utc
        )

        { status: :created, content: { shortcode: shortcode } }
      else
        { status: :unprocessable_entity }
      end
    end

    def stats(params)
      shortcode = @db.first(shortcode: params['shortcode'])

      return { status: :not_found } unless shortcode

      content = {
        startDate: shortcode[:created_at].iso8601,
        lastSeenDate: shortcode[:last_seen_at]&.iso8601,
        redirectCount: shortcode[:redirect_count]
      }.compact

      { status: :ok, content: content }
    end

    def show(params)
      shortcode = @db.first(shortcode: params['shortcode'])

      return { status: :not_found } unless shortcode

      @db.filter(id: shortcode[:id]).update(
        redirect_count: shortcode[:redirect_count] + 1,
        last_seen_at: Time.now.utc
      )

      { status: :found, headers: { 'Location' => shortcode[:url] } }
    end
  end
end
