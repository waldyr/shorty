# frozen_string_literal: true

require 'sequel'

require_relative 'lib/framework/router'
require_relative 'lib/framework/route'
require_relative 'lib/framework/app'

require_relative 'lib/shorty/urls_controller'

Sequel.default_timezone = :utc

DB = Sequel.connect(ENV.fetch('DATABASE_URL'))
DB.create_table?(:shortcodes) do
  primary_key :id

  column :shortcode, String, null: false
  column :url, String, null: false
  column :redirect_count, Integer, null: false, default: 0
  column :created_at, 'timestamp with time zone', null: false, default: Sequel::CURRENT_TIMESTAMP
  column :last_seen_at, 'timestamp with time zone'
end

urls_controller = Shorty::UrlsController.new(DB)

router = Framework::Router.new({
  'GET' => [
    Framework::Route.new('/:shortcode', urls_controller.method(:show)),
    Framework::Route.new('/:shortcode/stats', urls_controller.method(:stats))
  ],
  'POST' => [
    Framework::Route.new('/shorten', urls_controller.method(:shorten))
  ]
})

APP = Framework::App.new(router)
