# frozen_string_literal: true

require_relative '../test_helper'

module Framework
  class AppTest < ApplicationTest
    FAKE_ENV = nil

    def setup
      @router = Minitest::Mock.new
      @app = Framework::App.new(@router)
    end

    def test_app_router_serve_nil
      @router.expect(:serve, nil, [Rack::Request])
      expected = [404, { 'Content-Type' => 'application/json' }, ['{}']]
      assert_equal(expected, @app.call(FAKE_ENV))
    end

    def test_app_router_empty_hash
      @router.expect(:serve, {}, [Rack::Request])
      assert_raises(KeyError, 'key not found: :status') do
        @app.call(FAKE_ENV)
      end
    end

    def test_app_router_empty_hash
      @router.expect(:serve, { status: :server_error }, [Rack::Request])
      expected = [500, { 'Content-Type' => 'application/json' }, ['{}']]
      assert_equal(expected, @app.call(FAKE_ENV))
    end

    def test_app
      content = { a: 1, b: 2}
      serving = {
        status: :ok,
        headers: { 'Location' => 't' },
        content: content
      }

      @router.expect(:serve, serving, [Rack::Request])

      expected = [
        200,
        { 'Content-Type' => 'application/json', 'Location' => 't' },
        [content.to_json]
      ]

      assert_equal(expected, @app.call(FAKE_ENV))
    end
  end
end
