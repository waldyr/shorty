# frozen_string_literal: true

require_relative '../test_helper'

module Framework
  class RouteTest < ApplicationTest
    def test_match
      route = Framework::Route.new('/', ->(p){ 'OK' })

      request = Minitest::Mock.new
      request.expect(:path, '/')
      request.expect(:params, {})
      request.expect(:body, StringIO.new('{}'))

      assert_equal('OK', route.match(request))
    end

    def test_match_with_params
      route = Framework::Route.new('/', ->(p){ p['a'] + p['b'] })

      request = Minitest::Mock.new
      request.expect(:path, '/')
      request.expect(:params, { 'a' => 1 })
      request.expect(:body, StringIO.new('{ "b": 2 }'))

      assert_equal(3, route.match(request))
    end

    def test_match_without_matching
      route = Framework::Route.new('/', ->(p){ raise 'boom' })

      request = Minitest::Mock.new
      request.expect(:path, '/another')
      request.expect(:params, {})
      request.expect(:body, StringIO.new('{}'))

      assert_nil(route.match(request))
    end

    def test_match_with_exception
      route = Framework::Route.new('/', ->(p){ raise 'boom' })

      request = Minitest::Mock.new
      request.expect(:path, '/')
      request.expect(:params, {})
      request.expect(:body, StringIO.new('{}'))

      assert_raises(RuntimeError, 'boom') do
        route.match(request)
      end
    end

    def test_match_with_malformed_post_params
      route = Framework::Route.new('/', ->(p){ 'OK' })

      request = Minitest::Mock.new
      request.expect(:path, '/')
      request.expect(:params, {})
      request.expect(:body, StringIO.new('}{'))

      assert_equal('OK', route.match(request))
    end
  end
end
