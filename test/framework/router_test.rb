# frozen_string_literal: true

require_relative '../test_helper'

module Framework
  class RouterTest < ApplicationTest
    def test_router
      route = Minitest::Mock.new
      route.expect(:match, 'OK', [Minitest::Mock])

      router = Framework::Router.new({
        'GET' => [route]
      })

      request = Minitest::Mock.new
      request.expect(:request_method, 'GET')

      assert_equal('OK', router.serve(request))
    end

    def test_router_route_returning_falsey
      route = Minitest::Mock.new
      route.expect(:match, false, [Minitest::Mock])

      router = Framework::Router.new({
        'GET' => [route]
      })

      request = Minitest::Mock.new
      request.expect(:request_method, 'GET')

      assert_nil(router.serve(request))
    end

    def test_router_http_method_not_matching
      route = Minitest::Mock.new
      route.expect(:match, false, [Minitest::Mock])

      router = Framework::Router.new({
        'POST' => [route]
      })

      request = Minitest::Mock.new
      request.expect(:request_method, 'GET')

      assert_nil(router.serve(request))
    end
  end
end
