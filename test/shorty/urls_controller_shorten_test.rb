# frozen_string_literal: true

require_relative '../test_helper'

module Shorty
  class UrlsControllerShortenTest < ControllerTest
    def test_shorten
      transaction do
        response = post('/shorten', { url: 'http://example.com', shortcode: '1234' })

        assert_equal(201, response.status)
        assert_equal('application/json', response.content_type)

        shortcode = JSON.parse(response.body)['shortcode']
        assert_match(/\A[0-9a-zA-Z_]{4,}\z/, shortcode)
      end
    end

    def test_shorten_random
      transaction do
        response = post('/shorten', { url: 'http://example.com' })

        assert_equal(201, response.status)
        assert_equal('application/json', response.content_type)

        shortcode = JSON.parse(response.body)['shortcode']
        assert_match(/\A[0-9a-zA-Z_]{6}\z/, shortcode)
      end
    end

    def test_shorten_case_sensitiveness
      transaction do
        params = { url: 'http://example.com' }

        DB[:shortcodes].insert(params.merge(shortcode: 'example'))
        response = post('/shorten', params.merge(shortcode: 'EXAMPLE'))

        assert_equal(201, response.status)
        assert_equal('application/json', response.content_type)
      end
    end

    def test_shorten_without_url_in_params
      response = post('/shorten', {})

      assert_equal(400, response.status)
      assert_equal('application/json', response.content_type)

      assert_equal({}, JSON.parse(response.body))
    end

    def test_shorten_desired_shortcode_already_exists
      transaction do
        params = { url: 'http://example.com', shortcode: 'example' }

        DB[:shortcodes].insert(params)
        response = post('/shorten', params)

        assert_equal(409, response.status)
        assert_equal('application/json', response.content_type)

        assert_equal({}, JSON.parse(response.body))
      end
    end

    def test_shorten_unprocessable_shortcode
      response = post('/shorten', { url: 'http://example.com', shortcode: '' })

      assert_equal(422, response.status)
      assert_equal('application/json', response.content_type)

      assert_equal({}, JSON.parse(response.body))
    end
  end
end
