# frozen_string_literal: true

require_relative '../test_helper'

module Shorty
  class UrlsControllerShowTest < ControllerTest
    def test_show
      transaction do
        DB[:shortcodes].insert(url: 'http://example.com', shortcode: '1234')
        response = get('/1234')

        assert_equal(302, response.status)
        assert_equal('application/json', response.content_type)
        assert_equal('http://example.com', response.location)
        assert_equal({}, JSON.parse(response.body))
      end
    end

    def test_show_changing_count_and_last_seen
      transaction do
        params = { url: 'http://example.com', shortcode: '1234' }

        DB[:shortcodes].insert(params)

        5.times { get('/1234') }

        Time.stub(:now, Time.at(13)) do
          get('/1234')
        end

        count_and_last_seen = DB[:shortcodes].select(:redirect_count, :last_seen_at).first(params)
        assert_equal({ redirect_count: 6, last_seen_at: Time.at(13) }, count_and_last_seen)
      end
    end

    def test_show_not_found
      response = get('/1234')

      assert_equal(404, response.status)
      assert_equal('application/json', response.content_type)
      assert_nil(response.location)
      assert_equal({}, JSON.parse(response.body))
    end
  end
end
