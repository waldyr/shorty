# frozen_string_literal: true

require_relative '../test_helper'

module Shorty
  class UrlsControllerStatsTest < ControllerTest
    def test_stats
      transaction do
        DB[:shortcodes].insert(url: 'http://example.com', shortcode: '1234', created_at: '2018-08-26 20:39:46 +0200')
        response = get('/1234/stats')

        assert_equal(200, response.status)
        assert_equal('application/json', response.content_type)

        content = {
          'startDate' => '2018-08-26T18:39:46Z',
          'redirectCount' => 0
        }
        assert_equal(content, JSON.parse(response.body))
      end
    end

    def test_stats_with_redirect_count
      transaction do
        DB[:shortcodes].insert(
          url: 'http://example.com',
          shortcode: '1234',
          created_at: '2018-08-26 20:39:46 +0200',
          last_seen_at: '2018-08-30 20:39:46 +0200',
          redirect_count: 5
        )

        response = get('/1234/stats')

        assert_equal(200, response.status)
        assert_equal('application/json', response.content_type)

        content = {
          'startDate'=> '2018-08-26T18:39:46Z',
          'lastSeenDate' => '2018-08-30T18:39:46Z',
          'redirectCount' => 5
        }
        assert_equal(content, JSON.parse(response.body))
      end
    end

    def test_stats_not_found
      response = get('/1234/stats')

      assert_equal(404, response.status)
      assert_equal('application/json', response.content_type)
      assert_equal({}, JSON.parse(response.body))
    end
  end
end
