# frozen_string_literal: true

require 'minitest/autorun'
require 'rack/test'

ENV['RACK_ENV'] = 'test'

require_relative '../main'

class ApplicationTest < Minitest::Test
  parallelize_me!
end

class ControllerTest < ApplicationTest
  include Rack::Test::Methods

  protected

  def app
    APP
  end

  def transaction
    DB.transaction do
      yield

      raise Sequel::Rollback
    end
  end
end
